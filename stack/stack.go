package stack

import (
	"sync"
)

// Stack implements queue datastucture
type Stack struct {
	values []interface{}
	mutex  sync.Mutex
}

func NewStack() *Stack {
	return &Stack{}
}

func (s *Stack) Push(value interface{}) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	s.values = append(s.values, value)
}

func (s *Stack) Pop() interface{} {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	length := len(s.values)
	if length == 0 {
		return nil
	}
	value := s.values[length-1]
	s.values = s.values[:length-1]
	return value
}
