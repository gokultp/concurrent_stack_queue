package main_test

import (
	"testing"

	"gitlab.com/gokultp/concurrent_stack_queue/queue"
)

func TestEnqueue(t *testing.T) {
	q := queue.NewQueue()

	for i := 0; i < 1024; i++ {
		q.Enqueue(i)
	}
}
