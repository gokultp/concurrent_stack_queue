package main

import (
	"fmt"
	"sync"

	"gitlab.com/gokultp/concurrent_stack_queue/queue"
	"gitlab.com/gokultp/concurrent_stack_queue/stack"
)

func main() {
	var wgq sync.WaitGroup
	var wgs sync.WaitGroup

	q := queue.NewQueue()

	go func() {
		wgq.Add(1)
		q.Enqueue(1)
		wgq.Done()
	}()
	go func() {
		wgq.Add(1)
		q.Enqueue(2)
		wgq.Done()
	}()
	go func() {
		wgq.Add(1)
		q.Enqueue(3)
		wgq.Done()
	}()

	go func() {
		wgq.Add(1)
		fmt.Println(q.Dequeue())
		wgq.Done()
	}()
	go func() {
		wgq.Add(1)
		fmt.Println(q.Dequeue())
		wgq.Done()
	}()
	go func() {
		wgq.Add(1)
		fmt.Println(q.Dequeue())
		wgq.Done()
	}()

	s := stack.NewStack()

	go func() {
		wgs.Add(1)
		s.Push("a")
		wgs.Done()
	}()
	go func() {
		wgs.Add(1)
		s.Push("b")
		wgs.Done()
	}()
	go func() {
		wgs.Add(1)
		s.Push("c")
		wgs.Done()
	}()

	go func() {
		wgs.Add(1)
		fmt.Println(s.Pop())
		wgs.Done()
	}()
	go func() {
		wgs.Add(1)
		fmt.Println(s.Pop())
		wgs.Done()
	}()
	go func() {
		wgs.Add(1)
		fmt.Println(s.Pop())
		wgs.Done()
	}()

	wgq.Wait()
	wgs.Wait()

	fmt.Println(s, q)

}
