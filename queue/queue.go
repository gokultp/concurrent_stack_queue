package queue

import (
	"sync"
)

// Queue implements queue datastucture
type Queue struct {
	values []interface{}
	mutex  sync.Mutex
}

func NewQueue() *Queue {
	return &Queue{}
}

func (q *Queue) Enqueue(value interface{}) {
	q.mutex.Lock()
	defer q.mutex.Unlock()
	q.values = append(q.values, value)
}

func (q *Queue) Dequeue() interface{} {
	q.mutex.Lock()
	defer q.mutex.Unlock()
	if len(q.values) == 0 {
		return nil
	}
	value := q.values[0]
	q.values = q.values[1:]
	return value
}
